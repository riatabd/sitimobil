@extends('admin.layouts.master')

@section('content')

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Edit</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>

{!! Form::model($category, array('class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'PATCH', 'route' => array('admin.category.update', $category->id))) !!}

<div class="form-group">
    {!! Form::label('emri', 'Titulli*', array('class'=>'col-md-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('emri', old('emri',$category->emri), array('class'=>'form-control')) !!}
        
    </div>
</div>


<div class="form-group">
    {!! Form::label('prind', 'Zgjidh si nenkategori', ['class' => 'col-md-2 control-label']) !!}
     <div class="col-sm-6">
        <select class="form-control" id="prind" name="prind">
        <option value="0">- Zgjidh kategori -</option>
         @foreach($allSubCategories as $subCate)
            <option value="{{ $subCate->id }}" 
            @if($category->prind == $subCate->id) selected
           @endif
            >
                {{ $subCate->emri }}
                @foreach($subCate->subCategory as $firstNestedSub)
                    <option value="{{ $firstNestedSub->id }}" 
                   @if($firstNestedSub->parent->count()) disabled @endif>
                        - {{ $firstNestedSub->emri }}
                        @foreach($firstNestedSub->subCategory as $secondNestedSub)
                            <option value="{{ $secondNestedSub->id }}">
                            -- {{ $secondNestedSub->emri }}<br>
                                @foreach($secondNestedSub->subCategory as $thirdNestedSub)
                                    <option value="{{ $thirdNestedSub->id }}">
                                        ---{{ $thirdNestedSub->emri }}
                                    </option>
                                @endforeach()
                            </option>
                        @endforeach()
                    </option>
                @endforeach()
            </option>
            @endforeach()
        </select>
     </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {!! Form::submit('Update', array('class' => 'btn btn-primary')) !!}
      {!! link_to_route('admin.category.index', 'Cancel', $category->id, array('class' => 'btn btn-default')) !!}
    </div>
</div>


{!! Form::close() !!}

@endsection