@extends('admin.layouts.master')

@section('content')

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Edit</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>

{!! Form::model($product, array('files' => true, 'class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'PATCH', 'route' => array('admin.product.update', $product->id))) !!}

<div class="form-group">
    {!! Form::label('emri', 'Titulli*', array('class'=>'col-md-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('emri', old('emri',$product->emri), array('class'=>'form-control')) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('cmimi', 'Çmimi', array('class'=>'col-md-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('cmimi', old('cmimi',$product->cmimi), array('class'=>'form-control')) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('montimi', 'Montimi në minuta', array('class'=>'col-md-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('montimi', old('montimi',$product->montimi), array('class'=>'form-control')) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('fotografi', 'Fotografi', array('class'=>'col-md-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::file('fotografi') !!}
        {!! Form::hidden('fotografi_w', 1096) !!}
        {!! Form::hidden('fotografi_h', 1096) !!}
        
    </div>
</div>

<div class="form-group">
    {!! Form::label('category_id', 'Kategoria', ['class' => 'col-md-2 control-label']) !!}
     <div class="col-sm-6">
        <select class="form-control" id="category_id" name="category_id">
         @foreach($allSubCategories as $subCate)
            <option value="{{ $subCate->id }}" @if($subCate->id == $product->category_id) selected @endif
            @if($subCate->children->count()) disabled @endif>
                {{ $subCate->emri }}
                @foreach($subCate->subCategory as $firstNestedSub)
                    <option value="{{ $firstNestedSub->id }}" @if($firstNestedSub->id == $product->category_id) selected @endif>
                        - {{ $firstNestedSub->emri }}
                        @foreach($firstNestedSub->subCategory as $secondNestedSub)
                            <option value="{{ $secondNestedSub->id }}" @if($secondNestedSub->id == $product->category_id) selected @endif>
                            -- {{ $secondNestedSub->emri }}<br>
                                @foreach($secondNestedSub->subCategory as $thirdNestedSub)
                                    <option value="{{ $thirdNestedSub->id }}" @if($thirdNestedSub->id == $product->category_id) selected @endif>
                                        ---{{ $thirdNestedSub->emri }}
                                    </option>
                                @endforeach()
                            </option>
                        @endforeach()
                    </option>
                @endforeach()
            </option>
            @endforeach()
        </select>
     </div>
</div>

<div class="form-group">
    {!! Form::label('fotografi', 'Fotografi momentale', array('class'=>'col-md-2 control-label')) !!}
    <div class="col-sm-10">
      @if($product->fotografi != '')
            <a href="{{ asset('uploads/products') . '/'.  $product->fotografi }}" target="_blank"><img src="{{ asset('uploads/products/thumb') . '/'.  $product->fotografi }}"> </a>
        @endif 
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {!! Form::submit('Update', array('class' => 'btn btn-primary')) !!}
      {!! link_to_route('admin.product.index', 'Cancel', $product->id, array('class' => 'btn btn-default')) !!}
    </div>
</div>

{!! Form::close() !!}

@endsection