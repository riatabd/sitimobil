@extends('admin.layouts.master')

@section('content')

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1Add new</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>

{!! Form::open(array('files' => true, 'route' => 'admin.product.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}

<div class="form-group">
    {!! Form::label('emri', 'Titulli*', array('class'=>'col-md-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('emri', old('emri'), array('class'=>'form-control')) !!}
    </div>
</div><div class="form-group">
    {!! Form::label('cmimi', 'Çmimi', array('class'=>'col-md-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('cmimi', old('cmimi'), array('class'=>'form-control')) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('montimi', 'Montimi në minuta', array('class'=>'col-md-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('montimi', old('montimi'), array('class'=>'form-control')) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('fotografi', 'Fotografi', array('class'=>'col-md-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::file('fotografi' , array('class'=>'col-md-2 form-control')) !!}
        {!! Form::hidden('fotografi_w', 1096) !!}
        {!! Form::hidden('fotografi_h', 1096) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('category_id', 'Kategoria', ['class' => 'col-md-2 control-label']) !!}
     <div class="col-sm-6">
        <select class="form-control" id="category_id" name="category_id">
         @foreach($allSubCategories as $subCate)
            <option value="{{ $subCate->id }}" @if($subCate->children->count()) disabled @endif>
                {{ $subCate->emri }}
                @foreach($subCate->subCategory as $firstNestedSub)
                    <option value="{{ $firstNestedSub->id }}">
                        - {{ $firstNestedSub->emri }}
                        @foreach($firstNestedSub->subCategory as $secondNestedSub)
                            <option value="{{ $secondNestedSub->id }}">
                            -- {{ $secondNestedSub->emri }}<br>
                                @foreach($secondNestedSub->subCategory as $thirdNestedSub)
                                    <option value="{{ $thirdNestedSub->id }}">
                                        ---{{ $thirdNestedSub->emri }}
                                    </option>
                                @endforeach()
                            </option>
                        @endforeach()
                    </option>
                @endforeach()
            </option>
            @endforeach()
        </select>
     </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {!! Form::submit('Create', array('class' => 'btn btn-primary')) !!}
    </div>
</div>

{!! Form::close() !!}

@endsection