@include('front.partials.header')
@include('front.partials.topbar')

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            @include('front.partials.sidebar')

            <div class="col-md-9">
          
            @yield('content')   

            </div>

    </div>
    <!-- /.container -->

@include('front.partials.javascripts')

@yield('javascript')
@include('front.partials.footer')


