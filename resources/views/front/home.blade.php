@extends('front.layouts.master')

@section('content')

     <div class="row">
                @foreach ($product as $prod)

                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <img src="{{ url('uploads/products/thumb') }}/{{ $prod->fotografi }}" alt="">
                            <div class="caption">
                                <h4 class="pull-right" style="font-weight: bold">{{ $prod->cmimi }} Den</h4>
                                <h4><a href="#">{{ $prod->emri }}</a>
                                </h4>
                                <p>
                                <b>Kategoria:</b> {{ $prod->category->emri }}
                                </p>
                                <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                            </div>
                        </div>
                    </div>
                @endforeach


                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <h4><a href="#">Like this template?</a>
                        </h4>
                        <p>If you like this template, then check out <a target="_blank" href="http://maxoffsky.com/code-blog/laravel-shop-tutorial-1-building-a-review-system/">this tutorial</a> on how to build a working review system for your online store!</p>
                        <a class="btn btn-primary" target="_blank" href="http://maxoffsky.com/code-blog/laravel-shop-tutorial-1-building-a-review-system/">View Tutorial</a>
                    </div>

                </div>

            </div>

@endsection