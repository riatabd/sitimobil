
<div class="col-md-3">
	<div class="panel-group">
		
	  @foreach($allSubCategories as $subCate)
					<div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title">
				        @if($subCate->children->count())
				          <a data-toggle="collapse" href="#collapse{{ $subCate->id }}">{{ $subCate->emri }}
				          <span class="glyphicon glyphicon-collapse-down pull-right"></span>
				          </a>
				          	@else 
				          	<a href="{{url('kategoria')}}/{{ $subCate->id }}">
				          		{{ $subCate->emri }}
					        </a>
				          @endif
				        </h4>
				      </div>
				      @if($subCate->children->count())
					      <div id="collapse{{ $subCate->id }}" class="panel-collapse collapse">
					        <ul class="list-group">
					        @foreach($subCate->subCategory as $firstNestedSub)
					          <li class="list-group-item"><a href="{{url('kategoria')}}/{{ $firstNestedSub->id }}">{{ $firstNestedSub->emri }}</a></li>
					        @endforeach()
					        </ul>
					      </div>
					   @endif
				    </div>
	 	@endforeach()
	</div>
 </div>

