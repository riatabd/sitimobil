@extends('front.layouts.master')

@section('content')

     <div class="row">
                @foreach ($product as $prod)

                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <img src="{{ url('uploads/products/thumb') }}/{{ $prod->fotografi }}" alt="">
                            <div class="caption">
                                <h4 class="pull-right" style="font-weight: bold">{{ $prod->cmimi }} Den</h4>
                                <h4><a href="#">{{ $prod->emri }}</a>
                                </h4>
                                <p>
                                <b>Kategoria:</b> {{ $prod->category->emri }}
                                </p>
                                <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                            </div>
                        </div>
                    </div>
                @endforeach

                </div>

            </div>

@endsection