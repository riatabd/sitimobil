<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;




class Category extends Model {


    protected $table    = 'category';
    
    protected $fillable = ['emri', 'prind'];
    

    public static function boot()
    {
        parent::boot();

        Category::observe(new UserActionsObserver);
    }
    
     public function parent()
    {
        return $this->belongsTo('App\Category', 'prind');
    }

    public function children()
    {
        return $this->hasMany('App\Category', 'prind');
    }
    
}