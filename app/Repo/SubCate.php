<?php
    
    namespace App\Repo;
    use DB;

    /**
    * 
    */
    class SubCate
    {
        
        public function getCategories(){

            $categories=\App\Category::where('prind',0)->get();//united

            $categories=$this->addRelation($categories);

            return $categories;

        }

        public function getChild() {
            $categories=\App\Category::where('prind',0)->get();
            $categories=$this->addRelation($categories);
            return $categories;
        }

        protected function selectChild($id)
        {
            $categories=\App\Category::where('prind',$id)->get(); //rooney

            $categories=$this->addRelation($categories);

            return $categories;

        }

        protected function addRelation($categories){

            $categories->map(function ($item, $key) {
                
                $sub=$this->selectChild($item->id); 
                
                return $item=array_add($item,'subCategory',$sub);

            });

            return $categories;
        }
    }