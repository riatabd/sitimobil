<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;

class Product extends Model {

    protected $table    = 'product';
    
    protected $fillable = [
          'emri',
          'cmimi',
          'montimi',
          'fotografi',
          'category_id'
    ];
    

    public static function boot()
    {
        parent::boot();

        Product::observe(new UserActionsObserver);
    }
    
    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }
    
}