<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Product;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Category;
use App\Repo\SubCate;
use Input;
use Image;
use DB;


class ProductController extends Controller {

	/**
	 * Display a listing of product
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $product = Product::with("category")->get();

		return view('admin.product.index', compact('product'));
	}

	/**
	 * Show the form for creating a new product
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    $subcate = new SubCate;
        
        try {

            $allSubCategories = $subcate->getCategories();
            
        } catch (Exception $e) {
            
            //no parent category found
        }
	    
	    return view('admin.product.create', compact('allSubCategories','checkifsubcategory'));
	}

	/**
	 * Store a newly created product in storage.
	 *
     * @param CreateProductRequest|Request $request
	 */
	public function store(CreateProductRequest $request)
	{
	    
        //Uploading the product image
        if ($request->hasFile('fotografi')) {
                $file = $request->file('fotografi'); 

                // create an image
                $img = Image::make($file);
                $image_name = time()."-".$file->getClientOriginalName();
                
                // backup status
                $img->backup();

                // perform some modifications
                $img->fit(300);
                $img->save('uploads/products/thumb/'.$image_name);

                // reset image (return to backup state)
                $img->reset();

                // perform other modifications
                $img->save('uploads/products/'.$image_name);
        } else {
            $image_name = '';
        }
                

        $product = new Product;
        $product -> emri = $request->emri;
        $product -> cmimi = $request->cmimi;
        $product -> montimi = $request->montimi;
        $product -> category_id = $request->category_id;
        $product -> fotografi = $image_name;

        $product -> save();

        return redirect()->route('admin.product.index');
	}

	/**
	 * Show the form for editing the specified product.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$product = Product::find($id);
	    //$category = Category::lists("emri", "id")->prepend('Please select', '');

	    $subcate = new SubCate;
        
        try {

            $allSubCategories=$subcate->getCategories();
            
        } catch (Exception $e) {
            
            //no parent category found
        }
	    
		return view('admin.product.edit', compact('product', 'allSubCategories'));
	}

	/**
	 * Update the specified product in storage.
     * @param UpdateProductRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateProductRequest $request)
	{
        $product = Product::findOrFail($id);

		 //Uploading the product image
        if ($request->hasFile('fotografi')) {
                $file = $request->file('fotografi'); 

                // create an image
                $img = Image::make($file);
                $image_name = time()."-".$file->getClientOriginalName();
                
                // backup status
                $img->backup();

                // perform some modifications
                $img->fit(300);
                $img->save('uploads/products/thumb/'.$image_name);

                // reset image (return to backup state)
                $img->reset();

                // perform other modifications
                $img->save('uploads/products/'.$image_name);
        } else {
            $image_name=$product->fotografi;
        }

        $product -> emri = $request->emri;
        $product -> cmimi = $request->cmimi;
        $product -> montimi = $request->montimi;
        $product -> category_id = $request->category_id;
        $product -> fotografi = $image_name;

		$product->update();

		return redirect()->route('admin.product.index');
	}

	/**
	 * Remove the specified product from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Product::destroy($id);

		return redirect()->route('admin.product.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Product::destroy($toDelete);
        } else {
            Product::whereNotNull('id')->delete();
        }

        return redirect()->route('admin.product.index');
    }
}



