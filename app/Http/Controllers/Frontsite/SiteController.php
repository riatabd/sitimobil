<?php namespace App\Http\Controllers\Frontsite;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Product;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Category;
use App\Repo\SubCate;
use Input;
use Image;
use DB;


class SiteController extends Controller {
	/**
	 * Display a listing of product
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
        //$product = Product::with("category")->get();

        $subcate = new SubCate;
        
        try {
            
            $product = Product::with("category")->get();
            $allSubCategories = $subcate->getCategories();
            
        } catch (Exception $e) {
            
            //no parent category found
        }

		return view('front.home', compact('product','allSubCategories'));
	}

	public function show($id)
    {
        //$product = Product::with("category")->where("category_id",$id)->get();
        //$product = Product::find($id);

        $subcate = new SubCate;
        
        try {
            $product = Product::with("category")->where("category_id",$id)->get();

            $allSubCategories = $subcate->getCategories();
            
        } catch (Exception $e) {
            
            //no parent category found
        }

		return view('front.product.productcategory', compact('product','allSubCategories'));
	}
}